#联通 SDK 4.0.2 

###官方Host
http://unilog.wostore.cn:8061/logserver/unipay/unipayLog2

###中转Host
http://xxx.xxx.xxx.xxx:8080/unicom/402

###提交方式
POST

##### 中转登录请求
- send

| 参数  | 参考值 | 说明 |
| ------------- | ------------- | ------------- |
| logtype | login | 日志类型登录 |
| imsi | ------------- | ------------- |
| osversion | 4.4.4 | Build$VERSION.RELEASE<br/>手机系统版本 |
| model | Nexus 5 | Build.MODEL |
| imei | ------------- | ------------- |
| mac | ------------- | ------------- |
| userid | 654df17e18c7fe15c358a29014f0f2f0 | uuid	保存在文件中一台设备一个，生成方式参照代码 UUIDManager |
| appid | ------------- | ------------- |
| game | 游戏测试计费点 | UnicomConsume.uwc 文件解密出来的 app_name 字段的值 |
| channel | ------------- | 渠道号 |
| ip | ------------- | ------------- |
| net | WIFI | 参考代码 NetType.java |
| CPU_ABI | armeabi-v7a | CPU_ABI |
| CPU_ABI2 | armeabi | CPU_ABI2 |
| versionName | ------------- | PackageInfo.versionName |
| versionCode | ------------- | PackageInfo.versionCode |
| packagename | ------------- | ------------- |
| appname | 单机联网Demo | 你应用程序的名字，也就是 app_name 的值 |

- recv

| 参数  | 参考值 | 说明 |
| ------------- | ------------- | ------------- |
| content | ------------- | 返回加密后的内容 |
| restype | login | 返回请求的类型 |

##### 官方登录日志请求

- send
将中转 recv 返回的 content  内容全部 POST 发送给官方Host
- recv
成功返回 SUCCESS

##### 中转支付请求

- send

| 参数  | 参考值 | 说明 |
| ------------- | ------------- | ------------- |
| reqtype | pay |  日志类型支付 |
| pfdanji | 1 | 字符串<br/>是单机版本支付值为 1 <br/>联网版支付值为0 |
| net | WIFI | 参考代码 NetType.java |
| imsi | ------------- | ------------- |
| imei | ------------- | ------------- |
| appid | ------------- | ------------- |
| value | 0.1 | 价格，支付金额 |
| channel | ------------- | 渠道号 |
| vaccode | ------------- | vac code 付费代码 |
| cp_id | ------------- | 联通配置文件中的 cp_id |
| mac | xx : xx : xx : xx : xx : xx | ------------- |

- recv

| 参数  | 参考值 | 说明 |
| ------------- | ------------- | ------------- |
| content | ------------- | 返回加密后的内容 |
| restype | pay | 返回请求的类型 |
| smsContent | ------------- | sendDataMessage 发送短信的加密内容 |
| smsNum | 10655198018 | 发送短信的目标号码 |
| smsPort | 1 | 发送短信的端口号 |

##### 官方支付日志请求

同登录

