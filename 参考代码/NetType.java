 public static String getNetType(Context context) {
        String v0_1;
        NetworkInfo v0 = context.getSystemService("connectivity").getActiveNetworkInfo();
        if(v0 == null) {
            v0_1 = "NONET";
        }
        else if(v0.getType() == 1) {
            v0_1 = "WIFI";
        }
        else if(v0.getType() == 0) {
            switch(v0.getSubtype()) {
                case 0: {
                    return "unknow";
                }
                case 1: {
                    return "GPRS";
                }
                case 2: {
                    return "EDGE";
                }
                case 3: {
                    return "UMTS";
                }
                case 4: {
                    return "CDMA";
                }
                case 5: 
                case 6: {
                    return "EVDO";
                }
                case 7: {
                    return "1xRTT";
                }
                case 8: {
                    return "HSDPA";
                }
                case 9: {
                    goto label_31;
                }
                case 10: {
                    return "HSPA";
                }
                case 11: {
                    return "IDEN";
                }
            }

            return "unknow";
        label_31:
            v0_1 = "HSUPA";
        }
        else {
            v0_1 = "unknow";
        }

        return v0_1;
    }