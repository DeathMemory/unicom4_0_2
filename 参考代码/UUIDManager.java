package consoleTest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class UUIDManager {
	public static String createUUID() {
        return calcUUID(UUID.randomUUID().toString());
    }
	
	public static String calcUUID(String arg5) {
        String v0_3;
        try {
            MessageDigest v0_1 = MessageDigest.getInstance("MD5");
            v0_1.update(arg5.getBytes());
            byte[] md5UUID = v0_1.digest();
            StringBuffer v3 = new StringBuffer("");
            int v1;
            for(v1 = 0; v1 < md5UUID.length; ++v1) {
                int v0_2 = md5UUID[v1];
                if(v0_2 < 0) {
                    v0_2 += 256;
                }

                if(v0_2 < 16) {
                    v3.append("0");
                }

                v3.append(Integer.toHexString(v0_2));
            }

            v0_3 = v3.toString();
        }
        catch(NoSuchAlgorithmException v0) {
            v0.printStackTrace();
            v0_3 = "";
        }

        return v0_3;
    }
}
